Ekylibre pipeline utilities
---

# Rubygems
The folder contains a template for a CI job allowing to automatically (or manually) push a Gem to Rubygems.org

Based on the behavior you want, a different job template should be used:
  - If the release should be done by manually triggering the job through the Gitlab interface, use `.rubygems-manual`
  - If the the gem should be released automatically once all the verifications have been made, use `.rubygems-automatic`
  - If you want to provide your own set of rules for when the job should be added to the pipeline, use `.rubygems-base`.
    /!\ This template does not do any validation on the event that triggered the pipeline, its your job to provide the full condition.

For it to work:
  - The rubygems credentials have to be provided in an environment variable named $RUBYGEMS_CREDENTIALS.
    /!\ It should be a variable of type `File`.
  - The tag should be _exactly_ the same as the version of the gem that is being built
  - The tag should be a valid Semver version number. See https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
  - The name of the gem should be provided with the GEMSPEC variable. Its the name of the gemspec file without the extension.
  - The stage of the job should also be set.

## Warnings
Using this job template basically means that __anyone__ that has the ability to push a tag on the repository has ownership on the gem through rubygems.
Restriction on who can push new tags __MUST__ be added to the repository by
  - Protecting the creation of Semver tags
  - Protecting the rubygems credentials variable
  - Make sure only authorized users are able to access the CI/CD configuration page in gitlab